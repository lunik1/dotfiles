;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (ie. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '("~/.spacemacs.d/")
   ;; List of configuration layers to load. If it is the symbol `all' instead
   ;; of a list then all discovered layers will be installed.
   dotspacemacs-configuration-layers
   `(
     (auto-completion  :variables
                       auto-completion-enable-help-tooltip t
                       auto-completion-tab-key-behavior 'complete
                       auto-completion-enable-sort-by-usage t)
     better-defaults
     bibtex
     (c-c++ :variables
            c-c++-default-mode-for-headers 'c++-mode
            c-c++-enable-clang-support t)
     (colors :variables
             ;; colors-enable-nyan-cat-progress-bar t
             colors-colorize-identifiers 'all)
     emacs-lisp
     erc
     eyebrowse
     evil-commentary
     extra-langs
     ibuffer
     ivy
     git
     github
     latex
     markdown
     org
     python
     ranger
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom
            shell-default-term-shell "/usr/bin/zsh")
     spell-checking
     syntax-checking
     (theming :variables
              theming-headings-inherit-from-default 'all
              theming-headings-same-size 'all
              theming-headings-bold 'all)
     version-control
     vim-empty-lines
     unimpaired
     xkcd

     ,(unless (eq system-type 'windows-nt) 'cscope)

     ;; Non-contrib
     )
   ;; List of additional packages that will be installed wihout being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages then consider to create a layer, you can also put the
   ;; configuration in `dotspacemacs/config'.
   dotspacemacs-additional-packages '(ox-reveal pkgbuild-mode)
   ;; A list of packages and/or extensions that will not be install and loaded.
   dotspacemacs-excluded-packages '()
   ;; If non-nil spacemacs will delete any orphan packages, i.e. packages that
   ;; are declared in a layer which is not a member of
   ;; the list `dotspacemacs-configuration-layers'
   dotspacemacs-delete-orphan-packages t)
  )

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; Either `vim' or `emacs'. Evil is always enabled but if the variable
   ;; is `emacs' then the `holy-mode' is enabled at startup.
   dotspacemacs-editing-style 'vim
   ;; If non nil output loading progress in `*Messages*' buffer.
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed.
   dotspacemacs-startup-banner 'random
   ;; List of items to show in the startup buffer. If nil it is disabled.
   ;; Possible values are: `recents' `bookmarks' `projects'."
   dotspacemacs-startup-lists '(recents projects bookmarks)
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(darkokai
                         solarized-light)
   ;; If non nil the cursor color matches the state color.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font. `powerline-scale' allows to quickly tweak the mode-line
   ;; size to make separators look not too crappy.
   dotspacemacs-default-font `("Input Mono Narrow"
                               :size 14
                               :size ,(if (eq system-type 'windows-nt) 15 14)
                               :weight normal
                               :width normal
                               :powerline-scale 1)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The leader key accessible in `emacs state' and `insert state'
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it.
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; The command key used for Evil commands (ex-commands) and
   ;; Emacs commands (M-x).
   ;; By default the command key is `:' so ex-commands are executed like in Vim
   ;; with `:' and Emacs commands are executed with `<leader> :'.
   dotspacemacs-command-key "SPC"
   ;; If non nil then `ido' replaces `helm' for some commands. For now only
   ;; `find-files' (SPC f f) is replaced.
   dotspacemacs-use-ido nil
   ;; If non nil, `helm' will try to miminimize the space it uses. (default nil)
   dotspacemacs-helm-resize t
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header t
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content.
   dotspacemacs-enable-paste-micro-state t
   ;; Guide-key delay in seconds. The Guide-key is the popup buffer listing
   ;; the commands bound to the current keystrokes.
   dotspacemacs-which-key-delay 0.4
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil ;; to boost the loading time.
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up.
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX."
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'.
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'.
   dotspacemacs-inactive-transparency 90
   ;; If non nil unicode symbols are displayed in the mode line.
   dotspacemacs-mode-line-unicode-symbols nil
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters the
   ;; point when it reaches the top or bottom of the screen.
   dotspacemacs-smooth-scrolling nil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   dotspacemacs-smartparens-strict-mode nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil advises quit functions to keep server open when quitting.
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now.
   dotspacemacs-default-package-repository nil
   )
  )

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init'.  You are free to put any
user code."
  (setq-default
   ;; Miscellaneous
   vc-follow-symlinks t


   ;;Ranger
   ranger-override-dired t


   ;; Org and calendar
   org-bullets-bullet-list '("▣" "■" "●" "►" "▸")
   org-agenda-files (quote ("~/org"))
   org-agenda-diary-file "~/org/Diary.org"
   org-agenda-span 'fortnight
   org-agenda-tag-filter-preset '("-exclude")

   calendar-set-date-style 'european
   )
  ;; Use UK not US holidays
  ;; Some cleverness to work out bank holiday dates
  (defun holiday-christmas-bank-holidays ()
    (let ((m displayed-month)
          (y displayed-year))
      (increment-calendar-month m y -1)
      (when (>= m 10)
        (let ((d (calendar-day-of-week (list 12 25 y))))
          (cond ((= d 5)
                 (list (list (list 12 28 y)
                             "Boxing Day Bank Holiday")))
                ((= d 6)
                 (list (list (list 12 27 y)
                             "Boxing Day Bank Holiday")
                       (list (list 12 28 y)
                             "Christmas Day Bank Holiday")))
                ((= d 0)
                 (list (list (list 12 27 y)
                             "Christmas Day Bank Holiday"))))))))
  (defun holiday-new-year-bank-holiday ()
    (let ((m displayed-month)
          (y displayed-year))
      (increment-calendar-month m y 1)
      (when (<= m 3)
        (let ((d (calendar-day-of-week (list 1 1 y))))
          (cond ((= d 6)
                 (list (list (list 1 3 y)
                             "New Year's Day Bank Holiday")))
                ((= d 0)
                 (list (list (list 1 2 y)
                             "New Year's Day Bank Holiday"))))))))
  ;; Actually set the holidays
  (setq-default
   holiday-general-holidays
   '((holiday-fixed 1 1 "New Year's Day")
     (holiday-new-year-bank-holiday)
     (holiday-fixed 2 14 "Valentine's Day")
     (holiday-fixed 3 17 "St. Patrick's Day")
     (holiday-fixed 4 1 "April Fools' Day")
     (holiday-easter-etc -47 "Shrove Tuesday")
     (holiday-easter-etc -21 "Mother's Day")
     (holiday-easter-etc -2 "Good Friday")
     (holiday-easter-etc 0 "Easter Sunday")
     (holiday-easter-etc 1 "Easter Monday")
     (holiday-float 5 1 1 "Early May Bank Holiday")
     (holiday-float 5 1 -1 "Spring Bank Holiday")
     (holiday-float 6 0 3 "Father's Day")
     (holiday-float 8 1 -1 "Summer Bank Holiday")
     (holiday-fixed 10 31 "Halloween")
     (holiday-fixed 11 5 "Guy Fawkes Night")
     (holiday-float 11 0 2 "Rememberance Sunday")
     (holiday-fixed 11 11 "Armistice Day")
     (holiday-float 11 4 4 "Turkey Day")
     (holiday-fixed 12 24 "Christmas Eve")
     (holiday-fixed 12 25 "Christmas Day")
     (holiday-fixed 12 26 "Boxing Day")
     (holiday-christmas-bank-holidays)
     (holiday-fixed 12 31 "New Year's Eve"))

   holiday-other-holidays
   '((holiday-fixed 5 4 "Star Wars Day")
     (holiday-fixed 5 25 "Towel Day")
     (holiday-fixed 9 19 "International Talk Like a Pirate Day")
     (holiday-fixed 10 26 "Topin Wagglegammon"))

     holiday-christian-holidays nil
     holiday-bahai-holidays nil
     )
   )

(defun dotspacemacs/user-config ()
  "Configuration function.
 This function is called at the very end of Spacemacs initialization after
layers configuration."


  ;; Windows only stuff
  (when (eq system-type 'windows-nt)
    ;; Use MiKTeX's version of ghostscript to get doc-view working
    (setq doc-view-ghostscript-program "mgs")
    ;; On Windows, <menu> is <apps>
    (global-set-key (kbd "<apps>") 'smex)
    )

  ;; Keybinds
  (define-key evil-normal-state-map
    "\\" 'evil-repeat-find-char-reverse
    )
  (define-key evil-normal-state-map
    (kbd "<return>") (lambda ()
                       (interactive) (let ((oldpos (point))) (end-of-line)
                                          (newline-and-indent)))
    )
  (evil-leader/set-key "oa" 'org-agenda)


  ;; New client instances should open *spacemacs*, not *scratch*
  (spacemacs|do-after-display-system-init
   (setq initial-buffer-choice (lambda () (get-buffer spacemacs-buffer-name))))


  ;; Miscellaneous
  (setq-default
   tab-width 4
   )


  ;; IRC
  (setq-default
   erc-default-server "irc.desertbus.org"
   erc-autojoin-channels-alist
   '(("irc.desertbus.org" "#desertbus"))
   erc-nick "lunik1|emacs"
   erc-user-full-name "~senpai noticed me~"
   erc-email-userid "lunik1@0547b872.snorshbroadband.com"
   erc-keywords '("lunik*" "lun1k")
   )


  ;; Smartparens
  (remove-hook 'prog-mode-hook #'smartparens-mode)
  (spacemacs/toggle-smartparens-globally-off)

  ;; Appearance
  (add-hook 'prog-mode-hook 'linum-mode)
  (add-hook 'prog-mode-hook 'fci-mode)
  (add-hook 'text-mode-hook 'auto-fill-mode)

  (setq-default fci-rule-character ?│)

  (setq-default
   linum-format "%4d "
   powerline-default-separator 'arrow-fade
   )


  ;; Disable rainbow identifiers by default
  (remove-hook 'prog-mode-hook 'rainbow-identifiers-mode)
  (spacemacs/toggle-global-rainbow-identifiers-mode-off)


  ;; Text objects
  (spacemacs|define-text-object "/" "slash" "/" "/")
  (spacemacs|define-text-object "_" "underscore" "_" "_")
  (spacemacs|define-text-object "~" "tilde" "~" "~")
  (spacemacs|define-text-object "=" "equal" "=" "=")


  ;; TRAMP
  (unless (eq system-type 'windows-nt)
    (require 'tramp)
    ;; In TRAMP mode use ssh by default
    (setq-default tramp-default-method "ssh")
    )

  ;; Scrolling
  (setq-default
   mouse-wheel-scroll-amount '(3 ((shift) . 1) ((control) . nil))
   mouse-wheel-progressive-speed nil
   )
  ;; Enable scrolling with the mouse wheel in emacs-nox
  ;; (unless window-system
  ;;   (xterm-mouse-mode 1)
  ;;   (global-set-key [mouse-4] '(lambda ()
  ;;                                (interactive)
  ;;                                (scroll-down 3)))
  ;;   (global-set-key [mouse-5] '(lambda ()
  ;;                                (interactive)
  ;;                                (scroll-up 3))))


  ;; Flycheck
  (setq-default flycheck-display-errors-delay 0.6)
  (setenv "DICTIONARY" "en_GB")
  (cond
   ((executable-find "aspell")
    (setq ispell-program-name "aspell")
    (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_GB")))
   ((executable-find "hunspell")
    (setq ispell-program-name "hunspell")
    (setq ispell-extra-args '("-d en_GB")))
   )


  ;; Python
  ;; (setq-default flycheck-flake8rc ".flake8")


  ;; Some fixes for comint-style buffers
  (defmacro remove-from-list (list-var element)
    `(setq ,list-var (remove ,element ,list-var)))
  (dolist (mode '(erc-mode
                  comint-mode
                  term-mode
                  eshell-mode
                  inferior-emacs-lisp-mode)))

  (let ((comint-hooks '(eshell-mode-hook
                        term-mode-hook
                        erc-mode-hook
                        messages-buffer-mode-hook
                        comint-mode-hook)))
    (spacemacs/add-to-hooks (defun no-hl-line-mode ()
                              (setq-local global-hl-line-mode nil))
                            comint-hooks))
  (add-hook 'inferior-emacs-lisp-mode-hook 'smartparens-mode)


  ;; C++
  ;; Set style
  (setq c-default-style "bsd"
        tab-width 4
        c-basic-offset 4)

  ;; Syntax checking
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-clang-include-path
                             '("../include" "../inc"))))
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-gcc-include-path
                             '("../include" "../inc"))))

  ;; Use c++14
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-clang-language-standard "c++14")))
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-gcc-language-standard "c++14")))
  ;; Use the GNU standard library
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-clang-standard-library "libstdc++")))
  ;; Be pedantic
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-clang-pedantic-errors t)))
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-gcc-pedantic-errors t)))

  ;; Enable warnings
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-clang-warnings
                             '("everything"
                               "no-c++98-compat"
                               "no-c++98-compat-pedantic"
                               "no-padded"
                               "no-reserved-id-macro"
                               "no-unused-macros"))))
  (add-hook 'c++-mode-hook
            (lambda () (setq flycheck-gcc-warnings
                             '("all"
                               "extra"
                               "cast-align"
                               "cast-qual"
                               "ctor-dtor-privacy"
                               "format=2"
                               "init-self"
                               "inline"
                               "logical-op"
                               "missing-include-dirs"
                               "noexcept"
                               "old-style-cast"
                               "overloaded-virtual"
                               "redundant-decls"
                               "shadow"
                               "strict-null-sentinel"
                               "switch-default"
                               "useless-cast"
                               "virtual-dtor"
                               "zero-as-null-pointer-constant"))))
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
