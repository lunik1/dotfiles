filetype plugin indent on

"" BUNDLE

call plug#begin('~/.nvim/plugged')

Plug 'tomasr/molokai'

Plug 'benekastah/neomake'
  let g:neomake_cpp_clang_maker = {
    \ 'args': ['-std=c++14', '-pedantic', '-Weverything',
      \ '-Wno-c++98-compat', '-Wno-c++98-compat-pedantic',
      \ '-Wno-reserved-id-macro', '-Wno-unused-macros',
      \ '-Iinclude', '-I../include', 
      \ '-isystem/usr/include/root'],
    \ }
  let g:neomake_cpp_clangtidy_maker = {
    \ 'exe': 'clang-tidy',
    \ 'args': ['--checks=*,-google-readability-braces-around-statements,-readability-braces-around-statements,-clang-diagnostic-unused-macros,-clang-diagnostic-c++98*,-google-readability-todo',
      \ '%:p', '--',
      \ '-std=c++14', '-pedantic', '-Weverything',
      \ '-Wno-c++98-compat', '-Wno-c++98-compat-pedantic',
      \ '-Wno-reserved-id-macro', '-Wno-unused-macros',
      \ '-Iinclude', '-I../include', 
      \ '-isystem/usr/include/root'],
    \ }
  let g:neomake_cpp_gcc_maker = {
    \ 'args': ['-std=c++14', '-pedantic', '-Wall', '-Wextra', '-Wcast-align',
      \ '-Wcast-qual', '-Wctor-dtor-privacy', '-Wformat=2', '-Winit-self',
      \ '-Winline', '-Wlogical-op', '-Wmissing-include-dirs', '-Wnoexcept',
      \ '-Wold-style-cast', '-Woverloaded-virtual', '-Wredundant-decls',
      \ '-Wshadow', '-Wstrict-null-sentinel', '-Wswitch-default',
      \ '-Wzero-as-null-pointer-constant',
      \ '-Iinclude', '-I../include', 
      \ '-isystem/usr/include/root'],
    \ }
  let g:neomake_cpp_enabled_makers = ['gcc', 'clang', 'clangtidy']

  let g:neomake_python_flake8_maker = {
    \ 'args': ['--ignore="E701,F403"'],
    \ }
  let g:neomake_python_enabled_makers = ['python', 'flake8']
Plug 'bling/vim-airline'  " fancy command line
  let g:airline_powerline_fonts = 1
  "let g:airline#extensions#tabline#enabled = 1
  "let g:airline#extensions#tabline#buffer_nr_show = 1
  set laststatus=2
Plug 'bling/vim-bufferline'  " show buffers in the command line
Plug 'davidhalter/jedi-vim'
  let g:jedi#force_py_version = 3
  autocmd FileType python setlocal omnifunc=jedi#completions
  let g:jedi#completions_enabled = 0
  let g:jedi#auto_vim_configuration = 0
  let g:jedi#smart_auto_mappings = 0
  let g:jedi#show_call_signatures = 0
Plug 'JuliaLang/julia-vim'
Plug 'mbbill/undotree'  " undo tree navigation
  noremap <F6> :UndotreeToggle
Plug 'Shougo/deoplete.nvim'
  let g:deoplete#enable_at_startup = 1
Plug 'Shougo/unite.vim'
  nnoremap <silent> <Leader>h :Unite -buffer-name=recent -winheight=10 -start-insert file_mru<cr>
  nnoremap <Leader>b :Unite -buffer-name=buffers -winheight=10 -start-insert buffer<cr>
  nnoremap <silent> <Leader>f :Unite -start-insert -winheight=10 -buffer-name=files -winheight=10 file_rec/async:!<cr>
  nnoremap <Leader>s :Unite -winheight=10 grep:%<cr>
  nnoremap <Leader>S :Unite -winheight=10 grep:.<cr>
  if executable('ag')  " use ag
          let g:unite_source_grep_command = 'ag'
          let g:unite_source_grep_default_opts = '-i --nogroup --nocolor'
          let g:unite_source_grep_recursive_opt = ''
  endif
  nnoremap <Leader>z :Unite -winheight=10 -quick-match buffer<cr>
  let g:unite_source_history_yank_enable = 1
  nnoremap <Leader>y :Unite -winheight=10 history/yank<cr>
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'

call plug#end()

"""""""" 

behave xterm

colorscheme molokai
syntax on

set cursorline
set number
set spl=en_gb
set undofile
set hidden

set smarttab
set expandtab
set autoindent
set softtabstop=2
set shiftwidth=2

set foldmethod=syntax
set foldlevel=99

set cc=80

set fillchars+=vert:\ 

let g:netrw_browsex_viewer="xdg-open"

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1


" CUSTOM COMMANDS AND MAPPINGS
" ww writes and wipes current buffer
ca ww w<bar>bw 
" wd writes and deletes current buffer
ca wd w<bar>bd
" qq = wqa
ca qq wqa
" Enter creates a new line below, shift+Enter above
nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>
" wpy runs the file in Python (terminal)
ca wpy w<space>!python2
" wpy3 runs the file in Python 3 (terminal)
ca wpy3 w<space>!python3
" F5 forces a redraw
noremap <F5> :redraw!<cr>
" Disable paste on middle mouse click
map <MiddleMouse> <Nop>
imap <MiddleMouse> <Nop>

" autocmd BufEnter * silent! lcd %:p:h  " pwd is the file directory

" Delete empty buffer if opened in silent mode
if bufname('%') == ''
  set bufhidden=wipe
endif

" Automatically insert header guards
function! s:insert_gates()
        let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
        execute "normal! i#ifndef " . gatename
        execute "normal! o#define " . gatename
        execute "normal! o"
        execute "normal! o"
        execute "normal! Go#endif /* " . gatename . " */"
        normal! kk
endfunction
autocmd BufNewFile *.{h,hh,hxx,hpp} call <SID>insert_gates()
