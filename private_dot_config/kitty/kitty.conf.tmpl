# vim:fileencoding=utf-8:ft=conf

# Appearance
font_family        Iosevka Fixed Extended
italic_font        auto
bold_font          auto
bold_italic_font   auto
prefer_color_emoji yes

font_size       {{ .font_size }}
font_size_delta 1

adjust_line_height 0

cursor_shape     block
cursor_blink_interval     0.5
cursor_stop_blinking_after 15.0

background_opacity 1.0

url_style curly

# Colours
foreground           #ebdbb2
background           #1d2021

color0               #282828
color1               #cc241d
color2               #98971a
color3               #d79921
color4               #458588
color5               #b16286
color6               #689d6a
color7               #a89984
color8               #928374
color9               #fb4934
color10              #b8bb26
color11              #fabd2f
color12              #83a598
color13              #d3869b
color14              #8ec07c
color15              #ebdbb2

selection_foreground #1d2021
selection_background #ebdbb2
url_color            #ebdbb2
cursor               #ebdbb2

# Mouse
focus_follows_mouse yes

# Behaviour
enable_audio_bell no
use_system_wcwidth yes

# Windows
remember_window_size   no

# Key mapping
# Clipboard
map ctrl+shift+v        paste_from_clipboard
map ctrl+shift+s        paste_from_selection
map ctrl+shift+c        copy_to_clipboard
map shift+insert        paste_from_selection
map ctrl+shift+o        pass_selection_to_program

# Scrolling
map ctrl+shift+up        scroll_line_up
map ctrl+shift+down      scroll_line_down
map ctrl+shift+k         scroll_line_up
map ctrl+shift+j         scroll_line_down
map ctrl+shift+page_up   scroll_page_up
map ctrl+shift+page_down scroll_page_down
map ctrl+shift+home      scroll_home
map ctrl+shift+end       scroll_end
map ctrl+shift+h         show_scrollback

# Window management
map ctrl+shift+enter no_op
map ctrl+shift+n     no_op
map ctrl+shift+w     no_op
map ctrl+shift+]     no_op
map ctrl+shift+[     no_op
map ctrl+shift+f     no_op
map ctrl+shift+b     no_op
map ctrl+shift+`     no_op
map ctrl+shift+1     no_op
map ctrl+shift+2     no_op
map ctrl+shift+3     no_op
map ctrl+shift+4     no_op
map ctrl+shift+5     no_op
map ctrl+shift+6     no_op
map ctrl+shift+7     no_op
map ctrl+shift+8     no_op
map ctrl+shift+9     no_op
map ctrl+shift+0     no_op

# Tab management
map ctrl+shift+right no_op
map ctrl+shift+left  no_op
map ctrl+shift+t     no_op
map ctrl+shift+q     no_op
map ctrl+shift+l     no_op
map ctrl+shift+.     no_op
map ctrl+shift+,     no_op

# Miscellaneous
map ctrl+shift+equal     increase_font_size
map ctrl+shift+minus     decrease_font_size
map ctrl+shift+backspace restore_font_size
map ctrl+shift+f11       no_op
map ctrl+shift+u         input_unicode_character
