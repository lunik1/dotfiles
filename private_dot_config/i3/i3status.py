from i3pystatus import Status
from i3pystatus.updates import pacman, cower

status = Status(standalone=True)

# Clock in YYYY-MM-DD HH:MM:SS format
status.register("clock",
                format="  %F %T")

status.register("moon",
                format="{status}",
                status={"Waning Crescent": '🌘',
                        "New Moon": '🌑',
                        "Waning Gibbous": '🌖',
                        "First Quarter": '🌓',
                        "Last Quarter": '🌗',
                        "Waxing Crescent": '🌒',
                        "Waxing Gibbous": '🌔',
                        "Full Moon": '🌕'})

status.register("battery",
                format="{status}  {percentage_design:.0f}%",
                status={'DPL': '', 'DIS': '', 'CHR': '', 'FULL': ''},
                battery_ident="BAT0")

status.register("keyboard_locks",
                format="  {caps} {num}",
                caps_off="―――",
                num_off="―――")

status.register("disk",
                format="  {used} GiB ({percentage_used}%)",
                path="/",
                interval=60)

status.register("temp",
                format="  {temp:.0f}°C")

status.register("cpu_usage",
                format="  {usage:03d}%")

status.register("mem",
                format="♈ {used_mem:.0f} MiB")

status.register("openvpn",
                format="{status}",
                status_up="",
                status_down="",
                vpn_name="client")

status.register("network",
                format_up="  {essid} {quality:3}% {v4}",
                format_down="  down",
                interface="wlp2s0")

status.register("backlight",
                backlight="intel_backlight",
                interval=1,
                format=" {percentage:3}%")

status.register("pulseaudio",
                format=" {volume}%",
                format_muted="  {volume}%")

status.register("mpd",
                format="{status} {title} {song_elapsed}/{song_length}",
                status={"pause": '', "play": '', "stop": ''},
                hide_inactive=True,
                color="#EE00EE")

status.register("updates",
                format=" {count}",
                format_no_updates=" 0",
                backends=[pacman.Pacman(), cower.Cower()])

status.run()
