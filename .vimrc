set nocompatible              " be iMproved, required
" filetype off                  " required
" syntax enable

" set the runtime path to include NeoBundle and initialize
set runtimepath+=~/.vim/bundle/neobundle.vim/
call neobundle#begin(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim' " let NeoBundle manage Neobundle, required

NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'chriskempson/base16-vim'
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'sjl/badwolf'
NeoBundle 'tomasr/molokai'
NeoBundle 'vim-scripts/ScrollColors'  " :SCROLL to change colourschemes
" NeoBundle 'vim-scripts/wombat256.vim'
NeoBundle 'w0ng/vim-hybrid'

NeoBundle 'bling/vim-airline'  " fancy command line
        let g:airline_powerline_fonts = 1
        "let g:airline#extensions#tabline#enabled = 1
        "let g:airline#extensions#tabline#buffer_nr_show = 1
        set laststatus=2
NeoBundle 'bling/vim-bufferline'  " show buffers in the command line
NeoBundle 'hdima/python-syntax'  " better python syntax highlighting
        let g:python_version_2 = 1
        let g:python_highlight_all = 1
NeoBundleLazy 'mbbill/undotree'  " undo tree navigation
        noremap <F6> :UndotreeToggle
NeoBundleLazy 'mhinz/vim-startify'  " start screen and alternative session interface
        let g:startify_disable_at_vimenter = 1  " disable startscreen
NeoBundleLazy 'octol/vim-cpp-enhanced-highlight'
        let g:cpp_class_scope_highlight = 1
        " let g:cpp_experimental_template_highlight = 1
NeoBundleLazy 'scrooloose/nerdtree'  " file browser
NeoBundle 'scrooloose/syntastic'  " syntax checking
        let g:syntastic_aggregate_errors = 1
        let g:syntastic_mode_map = { 'mode': 'active',
                \ 'active_filetypes': [],
                \ 'passive_filetypes': [] } "these filetypes require manual checking
        let g:syntastic_cpp_checkers = ['clang_tidy', 'gcc']
        let g:syntastic_cpp_clang_tidy_args = "--checks='*,-google-readability-braces-around-statements,-readability-braces-around-statements,-clang-diagnostic-unused-macros,-clang-diagnostic-c++98*,-google-readability-todo'"
        let g:syntastic_cpp_clang_tidy_post_args = '-- -std=c++14 -Weverything -Wno-c++98-compat -Iinclude -I/usr/include/root'
        let g:syntastic_cpp_check_header = 1
        let g:syntastic_cpp_compiler_options = '-std=c++14 -Wall -Wextra -Wshadow -Wold-style-cast -Wswitch-default -Wzero-as-null-pointer-constant -Wuseless-cast -Winline -Wredundant-decls -Wcast-qual -Wcast-align -Wctor-dtor-privacy -Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wnoexcept -Woverloaded-virtual -Wstrict-null-sentinel -Wnon-virtual-dtor -pedantic -I/usr/include/root'
        let g:syntastic_python_checkers = ['flake8']
        let g:syntastic_python_flake8_args = '--ignore="E128,E701,F403"' "arguments to be passed to the syntax checker
NeoBundle 'Shougo/neocomplete.vim'
        let g:acp_enableAtStartup = 0
        let g:neocomplete#enable_at_startup = 1
        let g:neocomplete#enable_smart_case = 1
        let g:neocomplete#enable_auto_close_preview = 1
        let g:neocomplete#sources#syntax#min_keyword_length = 3
        let g:neocomplete#max_list = 20
        let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
        let g:necomplete#sources#dictionary#dictionaries = {
                \ 'default' : '',
                \ 'vimshell' : $HOME.'/.vimshell_hist',
                \ 'scheme' : $HOME.'/.gosh_completions'
                \ }
        if !exists('g:neocomplete#keyword_patterns')
                let g:neocomplete#keyword_patterns = {}
        endif
        let g:neocomplete#keyword_patterns['default'] = '\h\w*'
        autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
        autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
        autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
        autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
        autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
        if !exists('g:neocomplete#sources#omni#input_patterns')
            let g:neocomplete#sources#omni#input_patterns = {}
        endif
        if !exists('g:neocomplete#force_omni_input_patterns')
            let g:neocomplete#force_omni_input_patterns = {}
        endif
        let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
        " let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
        " let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
        inoremap <expr><Down>  pumvisible() ? "\<C-n>" : "\<Down>"
        inoremap <expr><Up>  pumvisible() ? "\<C-p>" : "\<Up>"
        inoremap <expr><Left>  pumvisible() ? "\<C-e>" : "\<Left>"
        inoremap <expr><Right>  pumvisible() ? "\<C-y>" : "\<Right>"
        if !exists('g:neocomplete#sources#omni#input_patterns')
                let g:neocomplete#sources#omni#input_patterns = {}
        endif
        NeoBundle 'c9s/perlomni.vim'
        let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
        NeoBundle 'osyo-manga/vim-marching'
                let g:marching_enable_neocomplete = 1
                let g:neocomplete#force_omni_input_patterns.cpp =
                                        \ '[^.[:digit:] *\t]\%(\.\|->\)\w*\|\h\w*::\w*'
        NeoBundle 'davidhalter/jedi-vim'
                let g:jedi#force_py_version = 2
                autocmd FileType python setlocal omnifunc=jedi#completions
                let g:jedi#completions_enabled = 1
                let g:jedi#auto_vim_configuration = 0
                let g:neocomplete#force_omni_input_patterns.python =
                            \ '\%([^. \t]\.\|^\s*@\|^\s*from\s.\+import \|^\s*from \|^\s*import \)\w*'
                let g:jedi#completions_enabled = 0
NeoBundle 'Shougo/neomru.vim'
NeoBundle 'Shougo/unite.vim'
        nnoremap <silent> <Leader>h :Unite -buffer-name=recent -winheight=10 -start-insert file_mru<cr>
        nnoremap <Leader>b :Unite -buffer-name=buffers -winheight=10 -start-insert buffer<cr>
        nnoremap <silent> <Leader>f :Unite -start-insert -winheight=10 -buffer-name=files -winheight=10 file_rec/async:!<cr>
        nnoremap <Leader>s :Unite -winheight=10 grep:%<cr>
        nnoremap <Leader>S :Unite -winheight=10 grep:.<cr>
        " if executable('ack-grep')  " use ack
        "         let g:unite_source_grep_command = 'ack-grep'
        "         let g:unite_source_grep_default_opts = '-i --no-heading --no-color -k -H'
        "         let g:unite_source_grep_recursive_opt = ''
        if executable('ag')  " use ag
                let g:unite_source_grep_command = 'ag'
                let g:unite_source_grep_default_opts = '-i --nogroup --nocolor'
                let g:unite_source_grep_recursive_opt = ''
        endif
        nnoremap <Leader>z :Unite -winheight=10 -quick-match buffer<cr>
        let g:unite_source_history_yank_enable = 1
        nnoremap <Leader>y :Unite -winheight=10 history/yank<cr>
NeoBundle 'Shougo/vimproc.vim', {
        \ 'build' : {
        \     'windows' : 'tools\\update-dll-mingw',
        \     'cygwin' : 'make -f make_cygwin.mak',
        \     'mac' : 'make -f make_mac.mak',
        \     'unix' : 'gmake',
        \     'linux' : 'make',
        \    },
        \ }
NeoBundle 'syngan/vim-vimlint', {
            \   'depends' : 'ynkdir/vim-vimlparser'}
NeoBundle 'tomtom/tcomment_vim'  " shortcuts to comment/uncomment lines
NeoBundle 'tpope/vim-abolish'  " coercion and foo{s,ing,bar}~K syntax
NeoBundle 'tpope/vim-fugitive'  " git integration
NeoBundle 'tpope/vim-repeat'  " . support for vim-abolish

NeoBundle 'artemkin/taglist.vim'
" NeoBundle 'techlivezheng/vim-plugin-minibufexpl'

" All of your Bundles must be added before the following line
call neobundle#end()
filetype plugin indent on    " required

NeoBundleCheck

" Put your non-Plugin stuff after this line

behave xterm

colorscheme molokai
syntax on

set backspace=2 "make backspace work like most other apps
" set clipboard=unnamedplus "use system clipboard
set cm=blowfish " use blowfish encryption
set cursorline
set encoding=utf-8 "use utf-8 in favour of latin 1
set guifont=Powerline\ Consolas
set guioptions-=T "hide toolbar
set guioptions-=m "hide menu bar
set guioptions-=r "hide right-hand scrollbar
set guioptions-=t "hide 'Tear off this menu' option
set guiheadroom=0 "don't have empty space in gvim when window size hints are ingnored
set hidden
set keymodel-=stopsel " make arrow keys work in visual mode
set mouse=a
" set noundofile "do not use undo file
set number
" set paste
set spl=en_gb
set t_Co=256 "Force the terminal to use 256 colours
set undofile "use the undo file

" Tab settings
set smarttab
set expandtab  " tabs are spaces
set autoindent "copy indent from previous line
set tabstop=4
set softtabstop=4
set shiftwidth=4

" Fold settings
set foldmethod=syntax "use syntax files to determine fold locations if possible
set foldlevel=99

let g:netrw_browsex_viewer= "xdg-open"

" URXVT STUFF
" Fix mode-dependent cursor in urxvt
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"
" Fix escape timeout in urxvt
set timeoutlen=1000 ttimeoutlen=0

" CUSTOM COMMANDS AND MAPPINGS
" ww writes and wipes current buffer
ca ww w<bar>bw 
" wd writes and deletes current buffer
ca wd w<bar>bd
" qq = wqa
ca qq wqa
" Enter creates a new line below, shift+Enter above
nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>
" wpy runs the file in Python (terminal)
ca wpy w<space>!python2
" wpy3 runs the file in Python 3 (terminal)
ca wpy3 w<space>!python3
" F5 forces a redraw
noremap <F5> :redraw!<cr>
" Disable paste on middle mouse click
map <MiddleMouse> <Nop>
imap <MiddleMouse> <Nop>

" autocmd BufEnter * silent! lcd %:p:h  " pwd is the file directory

" Delete empty buffer if opened in silent mode
if bufname('%') == ''
  set bufhidden=wipe
endif

" Automatically insert header guards
function! s:insert_gates()
        let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
        execute "normal! i#ifndef " . gatename
        execute "normal! o#define " . gatename
        execute "normal! o"
        execute "normal! o"
        execute "normal! Go#endif /* " . gatename . " */"
        normal! kk
endfunction
autocmd BufNewFile *.{h,hh,hxx,hpp} call <SID>insert_gates()
